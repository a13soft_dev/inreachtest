package com.example.inreachtest;

import android.app.Activity;
import android.app.TabActivity;
import android.os.Messenger;
import android.widget.TabHost;

import com.delorme.inreachapp.service.InReachService;
import com.delorme.inreachcore.InReachManager;

/**
 * Base activity with helper methods.
 * 
 * @author Eric Semle
 * @since inReachApp (07 May 2012)
 * @version 1.0
 * @bug AND-1009
 */
public class BaseTabActivity extends Activity
{
    /**
     * Returns the current InReachService. This will return null if
     * the service has not been started.
     * 
     * @author Eric Semle
     * @since inReachApp (07 May 2012)
     * @version 1.0
     * @bug AND-1009
     */
    public InReachService getInReachService()
    {
        final InReachApplication application = (InReachApplication) getApplication();
        final InReachService service = application.getService();
        
        return service;
    }
    
    /**
     * Returns the current InReachManager. This will return null if
     * the InReachService has not been started.
     * 
     * @author Eric Semle
     * @since inReachApp (07 May 2012)
     * @version 1.0
     * @bug AND-1009
     */
    public InReachManager getInReachManager()
    {
        final InReachService service = getInReachService();
        
        InReachManager manager = null;
        
        if(service != null)
        {
            manager = service.getManager();
        }
        
        return manager;
    }
    
    /**
     * Registers a messenger with the InReachService
     *      
     * @param messenger the client that receives InReachEvent messages.
     * @return True if successful, or false if the service is not started
     * or the messenger is null.
     * 
     * @author Eric Semle
     * @since inReachApp (07 May 2012)
     * @version 1.0
     * @bug AND-1009
     */
    public boolean registerMessenger(Messenger messenger)
    {
        if (messenger == null)
            return false;
        
        final InReachService service = getInReachService();
        
        if (service != null)
        {
            service.registerMessenger(messenger);
        }
        
        return (service != null);
    }
    
    /**
     * Unregisters a messenger from the InReachService
     * 
     * @param messenger the client that receives InReachEvent messages.
     * @return True if successful, or false if the service is not started
     * or the messenger is null.
     * 
     * @author Eric Semle
     * @since inReachApp (07 May 2012)
     * @version 1.0
     * @bug AND-1009
     */
    public boolean unregisterMessenger(Messenger messenger)
    {
        if (messenger == null)
            return false;
        
        final InReachService service = getInReachService();
        
        if (service != null)
        {
            service.unregisterMessenger(messenger);
        }
        
        return (service != null);
    }
    
    /**
     * A convince method for setting the tab.
     * 
     * @param tab the index of the tab to set.
     * 
     * @author Eric Semle
     * @since inReachApp (07 May 2012)
     * @version 1.0
     * @bug AND-1009
     */
    public void setCurrentTab(int tab)
    {
        final Activity parent = getParent();
        
        if (parent == null)
            return;
        
        TabActivity tabActivity = (TabActivity)parent;
        TabHost tabHost = tabActivity.getTabHost();
        tabHost.setCurrentTab(tab);
    }
}
