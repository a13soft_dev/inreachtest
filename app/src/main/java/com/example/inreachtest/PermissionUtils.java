package com.example.inreachtest;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

public class PermissionUtils {

    public static final int MULTIPLE_PERMISSIONS = 10;

    private static String[] permissions = new String[]{
            Manifest.permission.GET_ACCOUNTS,
            Manifest.permission.READ_CONTACTS,
            Manifest.permission.CAMERA,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.CALL_PHONE,
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.BLUETOOTH,
            Manifest.permission.BLUETOOTH_ADMIN,
    };

    private static List<String> listPermissionsNeeded;

    private PermissionUtils() {
        //no instance
    }

    public static void callPermissions(Activity activity) {
        ActivityCompat.requestPermissions(activity, permissions, MULTIPLE_PERMISSIONS);
    }

    public static boolean isAllPermissionGranted(Activity activity) {
        int result;
        listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(activity, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        return listPermissionsNeeded.size() == 0;
    }

    public static List<String> getListPermissionsNeeded() {
        return listPermissionsNeeded;
    }
}
