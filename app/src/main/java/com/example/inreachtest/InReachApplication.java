package com.example.inreachtest;

import com.delorme.inreachapp.service.InReachService;
import com.delorme.inreachapp.utils.LogEventHandler;

import android.app.Application;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.Messenger;

/**
 * A base class for maintaining the global state
 * of the InReachService.
 *
 * @author Eric Semle
 * @since inReachApp (07 May 2012)
 * @version 1.0
 * @bug AND-1009
 */
public class InReachApplication extends Application implements ServiceConnection
{
    interface ServiceCreatedCallback {
        void onServiceCreated();
    }

    public ServiceCreatedCallback serviceCreatedCallback = null;

    /**
     * Called when the application is create. This starts
     * the InReachService.
     *
     * @author Eric Semle
     * @since inReachApp (07 May 2012)
     * @version 1.0
     * @bug AND-1009
     */
    @Override
    public void onCreate()
    {
        super.onCreate();

        startService();
    }

    /**
     * Called when the application is terminated
     *
     * @author Eric Semle
     * @since inReachApp (07 May 2012)
     * @version 1.0
     * @bug AND-1009
     */
    @Override
    public void onTerminate()
    {
        super.onTerminate();

        stopService();
    }

    /**
     * Invoked when the service is binded
     *
     * @author Eric Semle
     * @since inReachApp (07 May 2012)
     * @version 1.0
     * @bug AND-1009
     */
    @Override
    public void onServiceConnected(ComponentName name, IBinder service)
    {
        m_service = ((InReachService.InReachBinder)service).getService();

        LogEventHandler handler = LogEventHandler.getInstance();
        m_messenger = new Messenger(handler);
        m_service.registerMessenger(m_messenger);
        if (serviceCreatedCallback != null) {
            serviceCreatedCallback.onServiceCreated();
        }
    }

    /**
     * Invoked when the service is disconnected
     *
     * @author Eric Semle
     * @since inReachApp (07 May 2012)
     * @version 1.0
     * @bug AND-1009
     */
    @Override
    public void onServiceDisconnected(ComponentName name)
    {
        if (m_service != null)
        {
            m_service.unregisterMessenger(m_messenger);
            m_service = null;
        }
    }

    /**
     * Returns the binded InReach Service
     *
     * @author Eric Semle
     * @since inReachApp (07 May 2012)
     * @version 1.0
     * @bug AND-1009
     */
    public InReachService getService()
    {
        return m_service;
    }

    /**
     * Starts the InReachService and binds it to the application
     *
     * @author Eric Semle
     * @since inReachApp (07 May 2012)
     * @version 1.0
     * @bug AND-1009
     */
    public void startService()
    {
        if (m_serviceStarted)
            return;

        Intent intent = new Intent(this, InReachService.class);

        startService(intent);
        bindService(intent, this, BIND_AUTO_CREATE);

        m_serviceStarted = true;
    }

    /**
     * Unbinds the InReachService and stops the service.
     *
     * @author Eric Semle
     * @since inReachApp (07 May 2012)
     * @version 1.0
     * @bug AND-1009
     */
    public void stopService()
    {
        if (!m_serviceStarted)
            return;

        Intent intent = new Intent(this, InReachService.class);

        unbindService(this);
        stopService(intent);

        m_serviceStarted = false;
    }

    /** Boolean flag as to whether or not the service has been started */
    public boolean m_serviceStarted = false;

    /** The bound inReach service */
    public InReachService m_service = null;

    /** The messenger for the LogEventHandler */
    public Messenger m_messenger = null;

    /** A handler for all inReach events that logs them */
    public LogEventHandler m_eventHandler = null;
}
